import java.util.ArrayList;
import java.util.Scanner;

public class Appuntamento {

  final private int giorno;
  final private int h_inizio;
  final private int h_fine;

  public Appuntamento(int giornoAppuntamento, int oraInizio, int oraFine) {
    if (noValidDay(giornoAppuntamento) || noValidHour(oraInizio) || noValidHour(oraFine) || oraInizio > oraFine)
      throw new IllegalArgumentException();

    giorno = giornoAppuntamento;
    h_inizio = oraInizio;
    h_fine = oraFine;
  }

  public static void main(String[] args) {
    var input = new Scanner(System.in);
    int gg, h1, h2;
    ArrayList<Appuntamento> agenda = new ArrayList<>();
    while (true) {
      try {
        gg = input.nextInt();
        h1 = input.nextInt();
        h2 = input.nextInt();
      } catch (Exception e) {
        break; // quando prova senza riuscirci a leggere un intero... termina input
      }

      try {
        Appuntamento a = new Appuntamento(gg, h1, h2);
        a.addAppuntamento(agenda);
      } catch (IllegalArgumentException e) {
        System.err.println("Fallita creazione appuntamento: " + gg + " " + h1 + " " + h2);
      }
    }
    System.out.println(agenda);
    input.close();
  }

  private boolean noValidDay(int giornoAppuntamento) {
    return 1 > giornoAppuntamento || giornoAppuntamento > 366;
  }

  private boolean noValidHour(int oraInizio) {
    return 0 > oraInizio || oraInizio > 24;
  }

  public void addAppuntamento(ArrayList<Appuntamento> agenda) {
    for (Appuntamento app : agenda)
      if (checkSovrapposizione(app))
        return;
    agenda.add(this);

  }

  private boolean checkSovrapposizione(Appuntamento appuntamentoPresente) {
    if (giorno != appuntamentoPresente.giorno)
      return false;

    if (h_inizio >= appuntamentoPresente.h_inizio && h_inizio < appuntamentoPresente.h_fine)
      return true;

    if (h_fine > appuntamentoPresente.h_inizio && h_fine <= appuntamentoPresente.h_fine)
      return true;

    return false;

  }

  @Override
  public String toString() {
    return "{" + giorno + "," + h_inizio + "," + h_fine + "}";
  }
}